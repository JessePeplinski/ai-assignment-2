/**
 *  Filename: nQueens.java
 *  Authors: Jesse Peplinski and Andrew Valancius
 *  Course: CIS 421 ­ Artificial Intelligence
 *  Assignment: 2
 *  Due: 10/5/2016, 11:00 PM
 **/

import java.io.*;
import java.util.*;

public class nQueens {

    private static Random rand = new Random();  // Random num generator

    /**
    *   Generates children from a collection of parents.
    *
    *   @param parents ArrayList of Individuals representing the parents
    *   
    *   @return Returns a collection of objects representing the new children
    **/
    public ArrayList<Individual> makeBabies(ArrayList<Individual> parents) {
        ArrayList<Individual> tempParents = new ArrayList<Individual>(parents); // Temp array listto hold parent (p1, p2) objects
        ArrayList<Individual> children = new ArrayList<Individual>();           // Children arraylist
        Collections.sort(tempParents); 

        Individual p1 = null;
        Individual p2 = null;

        // WHILE(At least 2 parents exist in the temp array)
        while (tempParents.size() > 1) {

            // Remove the parents from the temp array
            p1 = tempParents.remove(0);
            p2 = tempParents.remove(0);

            // Crossover the children
            children.addAll(crossOver(p1, p2));
        }

        // Iterate through the children and mutate them at a 10% probability
        for (Individual i : children) {
            if (rand.nextInt(100) <= 20) {
                i.mutate();
            }
        }
        return children;
    }

    /**
    *   Checks the genotype array to see if the value is contained within it
    *
    *   @param The genotype array, the crossover value, and the value you wish to check
    *   
    *   @return True if the value is found, false otherwise
    **/
    public boolean crossOverContains(int[] arrayToCheck, int crossOverPoint, int value) {
        for (int i = 0; i < crossOverPoint; i++) {
            if (arrayToCheck[i] == value) {
                return true;
            }
        }
        return false;
    }

    /**
    *   Crosses over the parents with the children
    *
    *   @param The two individuals, in this case the parents
    *   
    *   @return Arraylist of new children
    **/
    public ArrayList<Individual> crossOver(Individual p1, Individual p2) {
        int[] c1 = new int[p1.getGenoType().length];        // New int array for the genotypes
        int[] c2 = new int[p1.getGenoType().length];
        Arrays.fill(c1, -1);                               
        Arrays.fill(c2, -1);
        int crossOverPoint = rand.nextInt(p1.getGenoType().length);

        // Crossover the leftmost part of the crossover point for children 1 and 2, and parents 1 and 2
        for (int i = 0; i < crossOverPoint; i++) {
            c1[i] = p1.getGenoType()[i];
            c2[i] = p2.getGenoType()[i];
        }

        // Crossover the rightmost part of the crossover point for child 1 and parent 2
        for (int i = crossOverPoint; i < c1.length; i++) {
            if (!crossOverContains(c1, crossOverPoint, p2.getGenoType()[i])) {
                c1[i] = p2.getGenoType()[i];
            } else {
                for (int j = 0; j < p2.getGenoType().length; j++) {
                    if (!crossOverContains(c1, c1.length, p2.getGenoType()[j])) {
                        c1[i] = p2.getGenoType()[j];
                        break;
                    }
                }
            }
        }

        // Crossover the rightmost part of the crossover point for child 2 and parent 1
        for (int i = crossOverPoint; i < c2.length; i++) {
            if (!crossOverContains(c2, crossOverPoint, p1.getGenoType()[i])) {
                c2[i] = p1.getGenoType()[i];
            } else {
                for (int j = 0; j < p1.getGenoType().length; j++) {
                    if (!crossOverContains(c2, c2.length, p1.getGenoType()[j])) {
                        c2[i] = p1.getGenoType()[j];
                        break;
                    }
                }
            }
        }

        ArrayList<Individual> returnList = new ArrayList<Individual>();

        // Add new children to the arraylist to be returned
        returnList.add(new Individual(c1));
        returnList.add(new Individual(c2));

        // Print the crossover point
        System.out.println("Crossover is at " + crossOverPoint);

        // Print the parents genotypes
        System.out.println("Parent 1: " + p1.toString() + " with length " + p1.getGenoTypeLength());
        System.out.println("Parent 2: " + p2.toString() + " with length " + p2.getGenoTypeLength());

        // Print out the childrens newly generated genotype
        for(Individual i : returnList) {
            System.out.println("Child at: " + i.toString() + " with length " + i.getGenoTypeLength());
        }
        System.out.println();

        return returnList;
    }

    /**
    *   Run the main program
    * 
    **/
    public void run() {

        int N = 12;                         // The board size, Less than 100
        int POPULATION_SIZE = N * 10;       // The size of the population
        int MAX_GENERATIONS = 1000;         // Maximum number of generations before the program ends
        int generationCount = 0;            // Count of generations
        boolean solution = false;           
        FileWriter fw = null;
        PrintWriter writer = null;
        ArrayList<Individual> parents = new ArrayList<Individual>();
        ArrayList<Individual> matingPool = new ArrayList<Individual>();
        ArrayList<Individual> children = new ArrayList<Individual>();
        Population pop = new Population(POPULATION_SIZE, N);

        // Try to create the file readers
        try {
        	fw = new FileWriter("output.txt", true);
            writer = new PrintWriter(fw);
            writer.println("*************************************************");
            writer.println("*               NEW RUN OF PROGRAM              *");
            writer.println("*************************************************");
            writer.println("Generation Number    Solution Vector");
            writer.println("-----------------    ----------------------------");        
        }
        catch (Exception e) {
            System.out.println("output.txt not created");
        }
        
        while(generationCount < MAX_GENERATIONS) {
   
            System.out.println("\n\n\n\n");
            System.out.println("Generation count " + generationCount);

            // Parent Selection - Tournament Selection
            matingPool = pop.getNewMatingPool();

            // Generate offspring
            children = makeBabies(matingPool);

            // Survivor Selection / Population Update 
            pop.makeNewPop(children);

            // Increase the generation count
            generationCount++;

            // IF (The most fit individual is found)
            if (pop.getMostFit().getFitness() == 10000 ) {
                // Append results to file - Print the generation count and most fit genome
                writer.printf("%s                    %s\n", generationCount, pop.getMostFit());
                break;
            }
            if (generationCount == MAX_GENERATIONS - 1) {
                writer.printf("%s                    %s\n", generationCount, pop.getMostFit());
                break;
            }
            System.out.println("Highest fitness: " + pop.getMostFit().toString()+ " " + pop.getMostFit().getFitness());
        }
        writer.println("");  
        // Close the writer
     	writer.close();
    }

    /**
    *   Main function
    *
    **/
    public static void main(String[] args) {
        nQueens queen = new nQueens();
        queen.run();
    }
}