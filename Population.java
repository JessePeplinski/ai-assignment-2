/**
 *  Filename: Population.java
 *  Authors: Jesse Peplinski and Andrew Valancius
 *  Course: CIS 421 ­ Artificial Intelligence
 *  Assignment: 2
 *  Due: 10/5/2016, 11:00 PM
 */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Collections;
import java.util.Random;

public class Population {

    private static Random rand = new Random();
    private ArrayList<Individual> pop;
    private int popSize;


    /**
    * Constructor for the population of individuals.
    *
    * @param popSize Size of the population to create.
    * @param N Length of the genome of the individuals (board size).
    */
    public Population(int popSize, int N) {
        pop = new ArrayList<Individual>();
        this.popSize = popSize;

        for (int i = 0; i < popSize; i++) {
            pop.add(new Individual(N));

        }

    }

    /**
    *  Decides which individual is the most fit.
    *  
    *  @param temp1 First individual to compare
    *  @param temp2 Second individual to compare
    *  @param temp3 Third individual to compare
    *  
    *  @return the best individual of the three
    **/

    private Individual determineBestFitness(Individual temp1, Individual temp2, Individual temp3) {
        Individual bestIndividual = null;

        if (temp1.getFitness() >= temp2.getFitness() && temp1.getFitness() >= temp3.getFitness()) {
            //System.out.println("First temp has the best fitness");
            bestIndividual = temp1;
        }
        else if (temp2.getFitness() >= temp1.getFitness() && temp2.getFitness() >= temp3.getFitness()) {
            //System.out.println("Second temp has the best fitness");
            bestIndividual = temp2;
        }
        else if (temp3.getFitness() >= temp1.getFitness() && temp3.getFitness() >= temp2.getFitness()) {
            //System.out.println("Third temp has the best fitness");
            bestIndividual = temp3;
        }
        else {
            //System.out.println("Best fitness was not found, they must all be equal.");
            bestIndividual = temp1;
        }

        return bestIndividual;
    }


    /**
    *   Generates a collection of the most fit parents.
    *   
    *   @return Returns
    **/
    public ArrayList<Individual> getNewMatingPool() {
        ArrayList<Individual> newParents = new ArrayList<Individual>();
        ArrayList<Individual> tempPopulation = new ArrayList<Individual>(this.pop);

        while (newParents.size() <= this.pop.size()*0.1) {
            int pos1 = -1;
            int pos2 = -2;
            int pos3 = -3;
            do {
                pos1 = rand.nextInt(tempPopulation.size());
                pos2 = rand.nextInt(tempPopulation.size());
                pos3 = rand.nextInt(tempPopulation.size());
            } while (pos1 == pos2 || pos1 == pos3 || pos2 == pos3);

            Individual temp1 = tempPopulation.get(pos1);
            Individual temp2 = tempPopulation.get(pos2);
            Individual temp3 = tempPopulation.get(pos3);

            tempPopulation.remove(tempPopulation.indexOf(temp1));
            tempPopulation.remove(tempPopulation.indexOf(temp2));
            tempPopulation.remove(tempPopulation.indexOf(temp3));

            Individual winner = determineBestFitness(temp1, temp2, temp3);

            newParents.add(winner);

        }
        System.out.println("Size of new parents: " + newParents.size());
        for (Individual i : newParents) {
            System.out.println(i.toString());
        }
        return newParents;

    }

    public Individual getMostFit() {
        return pop.get(0);
    }

    /**
    *   Determines number of solutions in the population
    *
    *   @param children ArrayList of Individuals representing new children in the population.
    *   
    *   @return Returns number of different solutions.
    **/
    public void makeNewPop(ArrayList<Individual> children) {
        // Append the children to the pop list then sort it then drop the least
        // fit individuals until population size is back to what it was.
        this.pop.addAll(children);
        Collections.sort(this.pop);

        while (pop.size() > this.popSize) {
            pop.remove(pop.size() - 1);
        }

    }

    /**
    *   Determines number of solutions in the population
    *   
    *   @return Returns number of different solutions.
    **/
    public int numberOfSolutions() {
        HashSet<Individual> solutions = new HashSet<Individual>();
        for (Individual i : this.pop) {
            if (i.getFitness() == 10000) {
                solutions.add(i);
            }
        }
        return solutions.size();
    }

}