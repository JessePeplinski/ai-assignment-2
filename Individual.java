/**
 *  Filename: Individual.java
 *  Authors: Jesse Peplinski and Andrew Valancius
 *  Course: CIS 421 ­ Artificial Intelligence
 *  Assignment: 2
 *  Due: 10/5/2016, 11:00 PM
 **/

import java.lang.Math;
import java.util.ArrayList;
import java.util.Random;

public class Individual implements Comparable<Individual>{

    private static Random r = new Random(); 
    private final int N;                    // length of genotype
    private final int[] genoType;           // genotype int array to hold the values
    
    /**
    * Constructor for the Individual
    *
    * @param Population size of the population to create
    *
    **/
    public Individual(int n) {

        this.N = n;
        this.genoType = new int[n];
        for (int i = 0; i < n; i++) {
            this.genoType[i] = i;
        }

        for (int i = 0; i < n; i++) {
            this.mutate();
        }
    }

    /**
    * Constructor for the Individual
    *
    * @param The int array of genotype
    *
    **/
    public Individual(int[] genes) {
        this.N = genes.length;
        this.genoType = genes;
    }

    /**
    * Constructor for the Individual
    *
    * @param The individual object
    *
    **/
    public Individual(Individual i) {
        this.genoType = i.getGenoType();
        N = i.getGenoType().length;
    }

    /**
    * Get the int array of the genotype
    *
    * @return the int array
    *
    **/
    public int[] getGenoType() {
        return genoType;
    }

    /**
    * Return the length of the genotype
    *
    * @return the genotype length
    *
    **/
    public int getGenoTypeLength() {
        return N;
    }

    /**
    * Mutate the genotype by swapping 2 values randomly
    *
    **/
    public void mutate() {
        int pos1 = -1;
        int pos2 = -2;
        do {
            pos1 = r.nextInt(this.N);
            pos2 = r.nextInt(this.N);
        } while (pos1 == pos2);

        int temp1 = this.genoType[pos1];
        int temp2 = this.genoType[pos2];

        this.genoType[pos1] = temp2;
        this.genoType[pos2] = temp1;
    }

    /**
    * Get the fitness value of the current genotype by calculating conflicts
    * Looks at the slope values and if slops is ABS(1), a conflict exists
    *
    * @return the fitness of the genotype
    *
    **/
    public double getFitness() {
        double conflictCount = 0;
        double SMALL_CONSTANT = 0.0001;

        for (int i = 0; i < this.N; i++) {
            for (int j = 0; j < this.N; j++) {
                if (i != j) {
                    int x1 = i;
                    int y1 = this.genoType[i];
                    int x2 = j;
                    int y2 = this.genoType[j];

                    double rise = y2 - y1;
                    double run  = x2 - x1;

                    if (Math.abs(rise/run) == 1) {
                        conflictCount = conflictCount + 1;
                    }
                }
            }
        }
        return 1 / (conflictCount + SMALL_CONSTANT);
    }

    /**
    * It makes a hashcode
    *
    * @return returns a unique integer representing an instance of Individual
    *
    **/
    @Override
    public int hashCode() {
        long[] primes = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29,
         31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 
         89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 
         149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 
         199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 
         269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 
         337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 
         401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 
         463, 467, 479, 437, 491, 499, 503, 509, 521, 523, 541};
        long sum = 0;
        for (int i = 0; i < genoType.length; i++) {
            sum = sum + (genoType[i] * primes[i]);
        }
        return (int) sum;
    }

    /**
    * Compares the specified object with this Indivdual for equality.
    * If both objcts hashcode() functions return the same value this returns true.
    * 
    * @param obj the other object to compare with
    * @return true if they are equal, false otherwise
    *
    **/
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!Individual.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        final Individual i = (Individual) obj;
            if (this.hashCode() == i.hashCode()) return true;
        return false;
    }

    /**
    * Compares this object with the specified object for order. 
    * Returns a negative integer, zero, or a positive integer as 
    * this object is less than, equal to, or greater than the specified object.
    * 
    * @param The other Invidual to compare this with.
    * @return a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object.
    *
    **/
    @Override
    public int compareTo(Individual i) {
        //System.out.println(i.toString());
        if ((this.getFitness() - i.getFitness()) < 0) return 1;
        if ((this.getFitness() - i.getFitness()) > 0) return -1;
        return 0;
    }

    /**
    * Format the genotype nicely between open and closed bracketss
    * 
    * @return String of the genotype
    *
    **/
    @Override
    public String toString() {
        String returnStr = "<";
        for (int i : this.genoType) {
            returnStr = returnStr + i + " ";
        }
        returnStr = returnStr + ">";
        return returnStr;
    }

}