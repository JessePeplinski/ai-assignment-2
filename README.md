# Artificial Intelligence Assignment 2
# Genetic Algorithms - N-Queens

### Run 25 script
If you would like, we have a script that will remove the existing output file, compile all java files, and run the program 25 times.
First, `chmod u+x run25.sh`. Then, `./run25.sh`.

## Traditional compilation 
Type the command `javac *.java` to compile all java files. You can also compile each file manually if you would like.

### Tradtitionally running the project
To run the project, type the command `java nQueens`.

## Optional Compilation for Test Suite (This section is for our reference, feel free to ignore)

### Setting up jUnit
See this [link](http://stackoverflow.com/questions/21369953/need-help-installing-junit-on-mac-how-to-add-junit-to-path-environmental-variabl)

### Running tests
Compile the java files like normal, then run the tests with `java org.junit.runner.JUnitCore ClassName`. In this case, the class name is `TestBasic`. You can also follow [this tutorial](https://www.tutorialspoint.com/junit/junit_basic_usage.htm) to see how to create a main to run the tests. 

### Submiting project
On the lab machines, run `submit 421 *` to submit everything.